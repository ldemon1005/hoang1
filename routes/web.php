<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** @var \Illuminate\Support\Facades\Route $router */
$router->group(['namespace' => 'Admin', 'prefix' => 'admin'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */


    //<editor-fold desc="auth view">
    $router->get('/login', 'AuthController@loginView')->name('admin_auth_login_view');
    $router->get('/register', 'AuthController@registerView')->name('admin_auth_register_view');
    $router->get('/reset-password', 'AuthController@resetPassView')->name('admin_auth_reset_password_view');
    $router->get('/forget-password', 'AuthController@sendEmailForgotPassword')->name('admin_auth_forget_password_view');
    $router->get('/reset-password-client', 'AuthController@resetPassClientView')->name('admin_auth_reset_password_client_view');
    //</editor-fold>

    //<editor-fold desc="auth action">
    $router->post('/login-action', 'AuthController@loginAction')->name('admin_auth_login_action');
    $router->post('/register-action', 'AuthController@registerAction')->name('admin_auth_register_action');
    $router->post('/reset-password-action', 'AuthController@resetPasswordAction')->name('admin_auth_reset_password_action');
    $router->post('/change-email-action', 'AuthController@changeEmailAction')->name('admin_auth_change_email_action');
    $router->post('/logout', 'AuthController@logout')->name('admin_auth_logout');
    //</editor-fold>

    $router->group(['middleware' => ['auth.admin']], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        //<editor-fold desc="dashboard">
        $router::get('/dashboard', 'HomeController@dashboardV1')->name('dashboard-v1');
        //</editor-fold>

        //<editor-fold desc="category use view">
        $router->get('/list-category', 'CategoryController@listView')->name('admin_list_category');
        $router->get('/update-category/{id}', 'CategoryController@updateCategoryView')->name('admin_update_category_view');
        //</editor-fold>

        //<editor-fold desc="category action">
        $router->post('/create-category', 'CategoryController@createCategory')->name('admin_create_category');
        $router->post('/update-category', 'CategoryController@updateCategory')->name('admin_update_category_action');
        $router->post('/delete-category', 'CategoryController@deleteCategory')->name('admin_delete_category');
        //</editor-fold>

        //<editor-fold desc="category use view">
        $router->get('/list-category-room', 'CategoryRoomController@listView')->name('admin_list_category_room');
        $router->get('/update-category-room/{id}', 'CategoryRoomController@updateCategoryView')->name('admin_update_category_room_view');
        //</editor-fold>

        //<editor-fold desc="category action">
        $router->post('/create-category-room', 'CategoryRoomController@createCategory')->name('admin_create_category_room');
        $router->post('/update-category-room', 'CategoryRoomController@updateCategory')->name('admin_update_category_room_action');
        $router->post('/delete-category-room', 'CategoryRoomController@deleteCategory')->name('admin_delete_category_room');
        //</editor-fold>

        //<editor-fold desc="post use view">
        $router->get('/list-post', 'PostController@listView')->name('admin_list_post');
        $router->get('/update-post/{id}', 'PostController@updatePostView')->name('admin_update_post_view');
        //</editor-fold>

        //<editor-fold desc="post action">
        $router->post('/create-post', 'PostController@createPost')->name('admin_create_post_action');
        $router->post('/update-post', 'PostController@updatePost')->name('admin_update_post_action');
        $router->post('/delete-post', 'PostController@deletePost')->name('admin_delete_post');
        //</editor-fold>


        //<editor-fold desc="category service use view">
        $router->get('/list-category-service', 'CategoryServiceController@listView')->name('admin_list_category_service');
        $router->get('/update-category-service/{id}', 'CategoryServiceController@updateCategoryServiceView')->name('admin_update_category_service_view');
        //</editor-fold>

        //<editor-fold desc="category service action">
        $router->post('/create-category-service', 'CategoryServiceController@createCategoryService')->name('admin_create_category_service_action');
        $router->post('/update-category-service', 'CategoryServiceController@updateCategoryService')->name('admin_update_category_service_action');
        $router->post('/delete-category-service', 'CategoryServiceController@deleteCategoryService')->name('admin_delete_category_service');

        //<editor-fold desc="service use view">
        $router->get('/list-service', 'ServiceController@listView')->name('admin_list_service');
        $router->get('/update-service/{id}', 'ServiceController@updateServiceView')->name('admin_update_service_view');
        //</editor-fold>

        //<editor-fold desc="service action">
        $router->post('/create-service', 'ServiceController@createService')->name('admin_create_service_action');
        $router->post('/update-service', 'ServiceController@updateService')->name('admin_update_service_action');
        $router->post('/delete-service', 'ServiceController@deleteService')->name('admin_delete_service');
        //</editor-fold>

        //<editor-fold desc="contact use view">
        $router->get('/list-contact', 'ContactController@listView')->name('admin_list_contact');
        $router->get('/update-contact/{id}', 'ContactController@updateContactView')->name('admin_update_contact_view');
        //</editor-fold>

        //<editor-fold desc="contact action">
        $router->post('/update-contact', 'ContactController@updateContact')->name('admin_update_contact_action');
        $router->post('/delete-contact', 'ContactController@deleteContact')->name('admin_delete_contact');
        //</editor-fold>

        //<editor-fold desc="contact use view">
        $router->get('/list-order', 'OrderController@listView')->name('admin_list_order');
        $router->get('/update-order/{id}', 'OrderController@updateOrderView')->name('admin_update_order_view');
        //</editor-fold>

        //<editor-fold desc="order action">
        $router->post('/update-order', 'OrderController@updateOrder')->name('admin_update_order_action');
        $router->post('/delete-order', 'OrderController@deleteOrder')->name('admin_delete_order');
        //</editor-fold>


        //<editor-fold desc="config use view">
        $router->get('/update-config', 'ConfigController@updateConfigView')->name('admin_update_config_view');
        //</editor-fold>

        //<editor-fold desc="config action">
        $router->post('/update-config', 'ConfigController@updateConfig')->name('admin_update_config_action');
        //</editor-fold>
    });
});

$router->group(['namespace' => 'Admin', 'prefix' => 'api', 'middleware' => ['auth.admin']], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    /** ADMIN API */
    $router->post('/uploadImage', 'ApiController@uploadImage')->name('admin_api_image_upload');
});

$router->group(['namespace' => 'Client'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    //<editor-fold desc="auth view">
    $router->get('/', 'IndexController@index')->name('index_view');
    $router->get('/tim-kiem', 'IndexController@search')->name('search_view');
    $router->get('/gioi-thieu', 'IndexController@introduction')->name('introduction_view');
    $router->post('/lien-he-action', 'IndexController@contactAction')->name('contact_action');
    $router->get('/lien-he', 'IndexController@contactForm')->name('contact_view');
    $router->get('/tin-tuc', 'PostController@index')->name('list_post_view');
    $router->get('/tin-tuc/{slug}', 'PostController@detail')->name('detail_post_view');

    $router->get('/phong-nghi', 'RestroomController@index')->name('list_restroom_view');
    $router->get('/phong-nghi/{slug}', 'RestroomController@detail')->name('detail_restroom_view');
    $router->post('/dat-phong', 'RestroomController@createOrder')->name('order_restroom_view');

    $router->get('/phong-tiec', 'PartyroomController@index')->name('list_partyroom_view');
    $router->get('/phong-tiec/{slug}', 'PartyroomController@detail')->name('detail_partyroom_view');
    $router->post('/dat-phong-tiec', 'PartyroomController@createOrder')->name('order_partyroom_view');
    //</editor-fold>
});

$router->post('/create-device-client', 'Admin\DeviceController@createDevice')->name('client_create_device');
