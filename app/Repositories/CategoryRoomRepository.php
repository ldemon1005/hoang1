<?php

namespace App\Repositories;

use App\Models\CategoryRoom;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class CategoryRoomRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\CategoryRoom';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return CategoryRoom::where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->where('title','like','%' . $params['keyword'] . '%');
                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }
            })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getByID($category_room_id)
    {
        return CategoryRoom::where('id', '=', $category_room_id)->first();
    }

    public function findByCategoryRoom($category_room_name)
    {
        return CategoryRoom::where('name', '=', $category_room_name)->first();
    }

    /**
     * @param $params
     * @return CategoryRoom
     * @throws Exception
     */
    public function createCategory($params)
    {
        try {
            DB::beginTransaction();
            $params['created_at'] = time();
            $params['updated_at'] = time();
            $category_room = $this->create($params);
            DB::commit();
            return $category_room;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return CategoryRoom
     * @throws Exception
     */
    public function updateCategory($id, $params = [])
    {
        try {
            $params['updated_at'] = time();
            $updated_category_room = $this->update($params, $id);
            return $updated_category_room;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteCategory($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
