<?php

namespace App\Repositories;

use App\Models\Order;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class OrderRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Order';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return Order::where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->where('name','like','%' . $params['keyword'] . '%');
                    $query->orwhere('email','like','%' . $params['keyword'] . '%');
                    $query->orwhere('phone','like','%' . $params['keyword'] . '%');
                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }
            })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getByID($order_id)
    {
        return Order::where('id', '=', $order_id)->first();
    }

    public function findByOrder($order_name)
    {
        return Order::where('name', '=', $order_name)->first();
    }

    /**
     * @param $params
     * @return Order
     * @throws Exception
     */
    public function createOrder($params)
    {
        try {
            DB::beginTransaction();
            $params['created_at'] = time();
            $params['updated_at'] = time();
            $order = $this->create($params);
            DB::commit();
            return $order;
        } catch (Exception $e) {
            dd($e);
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Order
     * @throws Exception
     */
    public function updateOrder($id, $params = [])
    {
        try {
            $params['updated_at'] = time();
            $updated_order = $this->update($params, $id);
            return $updated_order;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteOrder($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
