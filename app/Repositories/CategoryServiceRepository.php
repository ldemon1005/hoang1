<?php

namespace App\Repositories;

use App\Models\CategoryService;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class CategoryServiceRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\CategoryService';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return CategoryService::where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->where('title','like','%' . $params['keyword'] . '%');
                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }

            if (!empty($params['type']) || isset($params['type'])) {
                $query->where('type', '=', intval($params['type']));
            }
            })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getByID($category_service_id)
    {
        return CategoryService::where('id', '=', $category_service_id)->first();
    }

    public function findByCategoryService($category_service_name)
    {
        return CategoryService::where('name', '=', $category_service_name)->first();
    }

    /**
     * @param $params
     * @return CategoryService
     * @throws Exception
     */
    public function createCategoryService($params)
    {
        try {
            DB::beginTransaction();
            $params['created_at'] = time();
            $params['updated_at'] = time();
            $params['slug'] = str_slug($params['title']);
            $category_service = $this->create($params);
            DB::commit();
            return $category_service;
        } catch (Exception $e) {
            dd($e);
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return CategoryService
     * @throws Exception
     */
    public function updateCategoryService($id, $params = [])
    {
        try {
            $params['updated_at'] = time();
            $params['slug'] = str_slug($params['title']);
            $updated_category_service = $this->update($params, $id);
            return $updated_category_service;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteCategoryService($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
