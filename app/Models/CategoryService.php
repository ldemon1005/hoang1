<?php

namespace App\Models;

class CategoryService extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'category_service';
}
