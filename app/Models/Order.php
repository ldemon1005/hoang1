<?php

namespace App\Models;

class Order extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'order';

    public function getCategory()
    {
        return $this->hasOne('App\Models\CategoryService',  'id', 'type');
    }

    public function getRank()
    {
        return $this->hasOne('App\Models\CategoryRoom',  'id', 'category_room');
    }
}
