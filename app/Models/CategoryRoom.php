<?php

namespace App\Models;

class CategoryRoom extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'category_room';
}
