<?php

namespace App\Models;

class Post extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'posts';

    public function getCategory()
    {
        return $this->hasOne('App\Models\Category',  'id', 'category');
    }
}
