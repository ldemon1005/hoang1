<?php

namespace App\Models;

class Service extends BaseModel
{
    //fix cứng theo ID
    const RESTROOM = 3;
    const PARTYROOM = 2;

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'service';

    public function getCategory()
    {
        return $this->hasOne('App\Models\CategoryService',  'id', 'category');
    }


}
