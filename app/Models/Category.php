<?php

namespace App\Models;

class Category extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'category';
}
