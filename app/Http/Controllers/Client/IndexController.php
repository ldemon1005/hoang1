<?php

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Models\Post;
use App\Repositories\CategoryRoomRepository;
use App\Repositories\CategoryServiceRepository;
use App\Repositories\ConfigRepository;
use App\Repositories\ContactRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\PostRepository;
use App\Repositories\PriceRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\TargetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class IndexController extends Controller
{
    private $configRepository;
    private $postRepository;
    private $categoryServiceRepository;
    private $contactRepository;
    private $serviceRepository;
    private $categoryRoomRepository;
    public function __construct(ConfigRepository $configRepository, PostRepository $postRepository,
                                CategoryServiceRepository $categoryServiceRepository, CategoryRoomRepository $categoryRoomRepository,
                                ContactRepository $contactRepository, ServiceRepository $serviceRepository)
    {
        $this->configRepository = $configRepository;
        $this->postRepository = $postRepository;
        $this->categoryServiceRepository = $categoryServiceRepository;
        $this->contactRepository = $contactRepository;
        $this->serviceRepository = $serviceRepository;
        $this->categoryRoomRepository = $categoryRoomRepository;

        $config = $this->configRepository->first();
        $config->banner_index = json_decode($config->banner_index);
        $config->banner_post = json_decode($config->banner_post);
        $list_service = $this->categoryServiceRepository->getList(['type' => 1,'status' => BaseModel::STATUS_ACTIVE],2);
        View::share([
            'config' => $config,
            'list_service' => $list_service
        ]);
    }

    public function index(){
        $posts = $this->postRepository->getList([], 12);
        $category_service = $this->categoryServiceRepository->getList(['status' => BaseModel::STATUS_ACTIVE, 'order_by' => 'order','order_direction' => 'asc']);
        $list_category_room = $this->categoryRoomRepository->getList(['status' => BaseModel::STATUS_ACTIVE]);
        return view('client.index.index', compact('posts','category_service', 'list_category_room'));
    }

    public function introduction(){
        $list_category_room = $this->categoryRoomRepository->getList(['status' => BaseModel::STATUS_ACTIVE]);
        return view('client.index.introduction',compact('list_category_room'));
    }

    public function contactForm(){
        return view('client.index.contact');
    }

    public function contactAction(Request $request){
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors('Gửi liên hệ không thành công!.Vui lòng xem lại dữ liệu nhập vào.');
        }

        $contact_data = $request->only('name','email','phone','content');

        $contact_data['status'] = BaseModel::STATUS_INACTIVE;

        try{
            $contact = $this->contactRepository->createContact($contact_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Gửi liên hệ không thành công!');
        }

        if(!$contact){
            return redirect()->back()->withErrors('Gửi liên hệ không thành công!');
        }

        return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Gửi liên hệ thành công!');
    }

    public function search(Request $request){
        $search_data = $request->only('search_type','keyword');
        if($search_data['search_type'] == 1){
            $posts = $this->postRepository->getList(['keyword' => $search_data['keyword'], 'status' => BaseModel::STATUS_ACTIVE]);
            return view('client.post.index', compact('posts'));
        }else{
            $services = $this->serviceRepository->getList(['keyword' => $search_data['keyword'], 'status' => BaseModel::STATUS_ACTIVE]);
            return view('client.index.search_service', compact('services'));
        }
    }
}
