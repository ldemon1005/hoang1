<?php

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Models\Post;
use App\Models\Service;
use App\Repositories\CategoryRoomRepository;
use App\Repositories\CategoryServiceRepository;
use App\Repositories\ConfigRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ServiceRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class PartyroomController extends Controller
{
    private $configRepository;
    private $partyRoomRepository;
    private $categoryRoomRepository;
    private $orderRepository;
    public function __construct(ConfigRepository $configRepository, ServiceRepository $partyRoomRepository, CategoryRoomRepository $categoryRoomRepository, OrderRepository $orderRepository)
    {
        $this->configRepository = $configRepository;
        $this->partyRoomRepository = $partyRoomRepository;
        $this->categoryRoomRepository = $categoryRoomRepository;
        $this->orderRepository = $orderRepository;

        $config = $this->configRepository->first();
        $config->banner_index = json_decode($config->banner_index);
        $config->banner_post = json_decode($config->banner_post);


        View::share([
            'config' => $config,
        ]);
    }

    public function index(){
        $list_category_room = $this->categoryRoomRepository->getList(['status' => BaseModel::STATUS_ACTIVE]);
        $partyRooms = Service::where('category',Service::PARTYROOM)->where('status',BaseModel::STATUS_ACTIVE)->paginate(8);
        return view('client.partyroom.index', compact('partyRooms','list_category_room'));
    }

    public function detail($slug = null){
        $params = explode('---',$slug);
        if(isset($params[1])){
            $partyRoom = $this->partyRoomRepository->getByID($params[1]);
            $list_category_room = $this->categoryRoomRepository->getList(['status' => BaseModel::STATUS_ACTIVE]);
            return view('client.partyroom.detail',compact('partyRoom','list_category_room'));
        }
        return redirect()->back()->withErrors('Vui lòng quay lại sau!');
    }

    public function createOrder(Request $request){
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors('Đặt phòng không thành công, vui lòng xem lại dữ liệu!');
        }

        $order_data = $request->only('name', 'email', 'phone', 'date_go', 'date_come', 'adults', 'children','category_room','number_night');
        $order_data['status'] = BaseModel::STATUS_INACTIVE;
        $order_data['type'] = Service::PARTYROOM;
        $order_data['date_go'] = strtotime($order_data['date_go']);
        $order_data['date_come'] = strtotime($order_data['date_come']);
        try{
            $order = $this->orderRepository->createOrder($order_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Đặt phòng không thành công, vui lòng quay lại sau!');
        }

        if(!$order){
            return redirect()->back()->withErrors('Đặt phòng không thành công, vui lòng quay lại sau!');
        }

        return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Đặt phòng thành công');
    }
}
