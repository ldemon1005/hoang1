<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Repositories\CategoryRepository;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class PostController extends Controller
{
    protected $postRepository;
    protected $categoryRepository;

    public function __construct(PostRepository $postRepository, CategoryRepository $categoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        View::share('type_menu', 'post');
    }

    public function listView(Request $request){
        $params = [
            'keyword' => '',
        ];
        $query = $request->only('keyword', 'status','category');
        $params = array_merge($params, $query);
        if(isset($params['status']) && $params['status'] == 9){
            unset($params['status']);
        }
        $list_post = $this->postRepository->getList($params);
        $list_category = $this->categoryRepository->getList(['status' => BaseModel::STATUS_ACTIVE]);
        return view('admin.post.list_view', compact('list_post','params','list_category'));
    }

    public function updatePostView($id = null){
        $post = null;
        if($id != 0){
            $post = $this->postRepository->getByID($id);

            if(!$post){
                $this->resFail(null, 'Không tìm bài viết');
            }
        }
        $list_category = $this->categoryRepository->getList(['status' => BaseModel::STATUS_ACTIVE]);

        return view('admin.post.form', compact('post','list_category'));
    }

    public function createPost(Request $request){
        $validator = Validator::make($request->input(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $post_data = $request->only('id','title','status', 'content','description', 'seo_title','seo_description','seo_keyword','image','category');

        if(isset($post_data['status']) && $post_data['status'] == 'on'){
            $post_data['status'] = 1;
        }else{
            $post_data['status'] = 0;
        }

        try{
            $post = $this->postRepository->createPost($post_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        if(!$post){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        return redirect()->route('admin_list_post')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    public function updatePost(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'title' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $post_data = $request->only('id','title','status', 'content','description', 'seo_title','seo_description','seo_keyword','image','category');

        $post_id = $request->get('id');

        unset($post_data['id']);

        if(isset($post_data['status']) && $post_data['status'] == 'on'){
            $post_data['status'] = 1;
        }else{
            $post_data['status'] = 0;
        }

        try{
            $post = $this->postRepository->updatePost($post_id, $post_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$post){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->route('admin_list_post')->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

    public function deletePost(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }
        $post_data = $request->only('id');

        try {
            $post_deleted = $this->postRepository->deletePost($post_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$post_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($post_deleted, 'Xoá thành công!');

    }
}
