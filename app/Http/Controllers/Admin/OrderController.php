<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Repositories\CategoryRoomRepository;
use App\Repositories\CategoryServiceRepository;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class OrderController extends Controller
{
    protected $orderRepository;
    protected $categoryRoomRepository;
    protected $categoryServiceRepository;

    public function __construct(OrderRepository $orderRepository, CategoryRoomRepository $categoryRoomRepository, CategoryServiceRepository $categoryServiceRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->categoryRoomRepository = $categoryRoomRepository;
        $this->categoryServiceRepository = $categoryServiceRepository;
        View::share('type_menu', 'order');
    }

    public function listView(Request $request){
        $params = [
            'keyword' => '',
        ];
        $query = $request->only('keyword', 'status');
        $params = array_merge($params, $query);
        if(isset($params['status']) && $params['status'] == 9){
            unset($params['status']);
        }
        $list_order = $this->orderRepository->getList($params);
        return view('admin.order.list_view', compact('list_order','params'));
    }

    public function updateOrderView($id){
        $order = $this->orderRepository->getByID($id);
        $list_category_service = $this->categoryServiceRepository->getList();
        $list_category_room = $this->categoryRoomRepository->getList();
        if(!$order){
            $this->resFail(null, 'Không tìm thấy thiết bị');
        }

        $content = view('admin.order.form', compact('order','list_category_room','list_category_service'))->render();

        return $this->resSuccess($content, '');
    }

    public function updateOrder(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $order_data = $request->only('id','status','name','email','phone','date_come','date_go','category_room','type','adults','children');

        $order_id = $request->get('id');

        unset($order_data['id']);

        if(isset($order_data['status']) && $order_data['status'] == 'on'){
            $order_data['status'] = 1;
        }else{
            $order_data['status'] = 0;
        }
        $order_data['date_go'] = strtotime($order_data['date_go']);
        $order_data['date_come'] = strtotime($order_data['date_come']);
        try{
            $order = $this->orderRepository->updateOrder($order_id, $order_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$order){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->route('admin_list_order')->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

    public function deleteOrder(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $order_data = $request->only('id');

        try {
            $order_deleted = $this->orderRepository->deleteOrder($order_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$order_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($order_deleted, 'Xoá thành công!');

    }
}
