<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Repositories\CategoryServiceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CategoryServiceController extends Controller
{
    protected $categoryServiceRepository;

    public function __construct(CategoryServiceRepository $categoryServiceRepository)
    {
        $this->categoryServiceRepository = $categoryServiceRepository;
        View::share('type_menu', 'service');
    }

    public function listView(Request $request){
        $params = [
            'keyword' => '',
        ];
        $query = $request->only('keyword', 'status');
        $params = array_merge($params, $query);
        if(isset($params['status']) && $params['status'] == 9){
            unset($params['status']);
        }
        $list_categoryService = $this->categoryServiceRepository->getList($params);
        return view('admin.categoryService.list_view', compact('list_categoryService','params'));
    }

    public function updateCategoryServiceView($id){
        $categoryService = null;
        if($id != 0){
            $categoryService = $this->categoryServiceRepository->getByID($id);

            if(!$categoryService){
                $this->resFail(null, 'Không tìm danh mục');
            }
        }

        return view('admin.categoryService.form', compact('categoryService'));
    }

    public function createCategoryService(Request $request){
        $validator = Validator::make($request->input(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $categoryService_data = $request->only('title','status','image','order','content');

        if(isset($categoryService_data['status']) && $categoryService_data['status'] == 'on'){
            $categoryService_data['status'] = 1;
        }else{
            $categoryService_data['status'] = 0;
        }
        try{
            $categoryService = $this->categoryServiceRepository->createCategoryService($categoryService_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        if(!$categoryService){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        return redirect()->route('admin_list_category_service')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    public function updateCategoryService(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $categoryService_data = $request->only('id','title','status', 'image','order','content');

        $categoryService_id = $request->get('id');

        unset($categoryService_data['id']);

        if(isset($categoryService_data['status']) && $categoryService_data['status'] == 'on'){
            $categoryService_data['status'] = 1;
        }else{
            $categoryService_data['status'] = 0;
        }

        try{
            $categoryService = $this->categoryServiceRepository->updateCategoryService($categoryService_id, $categoryService_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$categoryService){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->route('admin_list_category_service')->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

    public function deleteCategoryService(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }
        $categoryService_data = $request->only('id');

        try {
            $categoryService_deleted = $this->categoryServiceRepository->deleteCategoryService($categoryService_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$categoryService_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($categoryService_deleted, 'Xoá thành công!');

    }
}
