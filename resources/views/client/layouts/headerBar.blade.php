<div id="header-top">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div id="header-left">
          <ul>
            <li class="map"><img src="{{asset('client/images/map.png')}}"/></li>
            <li class="address">{{$config && $config->address != '' ? $config->address : '37 Hùng Vương, Ba Đình, Hà Nội'}}</li>
          </ul>
        </div>
        <div id="header-right">
            <ul>
                <li class="flag"><img src="{{asset('client/images/flag.jpg')}}"></li>
                <li class="search"><a href="#" class="btn-search"><img src="{{asset('client/images/search.png')}}"></a></li>
            </ul>
            <form method="get" action="{{route('search_view')}}" class="form-search">
                <label>Chọn loại: </label>
                <select name="search_type">
                    <option value="1">Tin tức</option>
                    <option value="2">Dịch vụ</option>
                </select>
                <input type="text" name="keyword" class="form-control" placeholder="Từ khóa ...">
                <button type="submit"><img src="{{asset('client/images/search.png')}}"></button>
                <button type="button" class="close" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="header-middle" class="desktop">
    <div class="container">
        <div class="row menu">
              <div class="col-md-2 text-center">
                  <div id="colorlib-logo">
                      <a href="{{url('/')}}" title="Nhà Khách Hùng Vương">
                          <img src="{{asset('client/images/logo.jpg')}}" title="Nhà Khách Hùng Vương">
                      </a>
                  </div>
              </div>
              <div class="col-md-5 order-first text-right menu-1">
                  <ul>
                      <li><a href="{{route('index_view')}}" title="trang chủ">Trang chủ</a></li>
                      <li><a href="{{route('introduction_view')}}" title="giới thiệu">Giới thiệu</a></li>
                      <li><a href="{{route('list_post_view')}}" title="tin tức">Tin tức</a></li>
                  </ul>
              </div>
              <div class="col-md-5 order-last text-left menu-1">
                  <ul>
                    <li><a href="{{route('list_partyroom_view')}}" title="tiệc">Tiệc</a></li>
                    <li><a href="{{route('list_restroom_view')}}" title="phòng nghỉ">Phòng nghỉ</a></li>
                    <li><a href="{{route('contact_view')}}" title="liên hệ">Liên hệ</a></li>
                  </ul>
              </div>
        </div>
    </div>
</div>
<div class="mobile">
  <div class="menu-mobile">
    <a href="{{url('/')}}" title="Nhà Khách Hùng Vương" class="logo-mobile">
        <img src="{{asset('client/images/logo.jpg')}}" title="Nhà Khách Hùng Vương">
    </a>
    <h1>Nhà khách Hùng Vương</h1>
    <a class="toggle-menu" style="color: #000;">
      <img src="{{asset('client/images/menu-bar.PNG')}}" alt="Nhà Khách Hùng Vương">
    </a>
  </div>
  <nav id="main-nav" class="mobile">
    <ul class="first-nav">
      <li>
        
      </li>
    </ul>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active"><a class="nav-link" href="{{route('index_view')}}" title="trang chủ">Trang chủ <span class="sr-only">(current)</span></a></li>
        <li><a class="nav-link" href="{{route('introduction_view')}}" title="giới thiệu">Giới thiệu</a></li>
        <li><a class="nav-link" href="{{route('list_post_view')}}" title="tin tức">Tin tức</a></li>
        <li><a class="nav-link" href="{{route('list_partyroom_view')}}" title="tiệc">Tiệc</a></li>
        <li><a class="nav-link" href="{{route('list_restroom_view')}}" title="phòng nghỉ">Phòng nghỉ</a></li>
        <li><a class="nav-link" href="{{route('contact_view')}}" title="liên hệ">Liên hệ</a></li>
    </ul>
  </nav>
</div>