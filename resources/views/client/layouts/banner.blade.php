<div id="slide" class="carousel slide" data-ride="carousel">
    <img src="{{asset('client/images/circle-vice.png')}}" class="circle-vice">
    <!-- Indicators -->
    <ul class="carousel-indicators">
        @foreach($config->banner_index as $key => $banner_index)
        <li data-target="#slide" data-slide-to="{{$key}}" class="{{$key == 0 ? 'active' : ''}}"></li>
        @endforeach
    </ul>
    <!-- The slideshow -->
    <div class="carousel-inner">
        @if($config && count($config->banner_index))
            @foreach($config->banner_index as $key => $banner_index)
                <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                    <img src="{{$banner_index}}" alt="banner {{$key}}">
                </div>
            @endforeach
        @else
            <div class="carousel-item active">
                <img src="{{asset('client/images/slide.jpg')}}" alt="Nhà khách Hùng Vương 1">
            </div>
            <div class="carousel-item">
                <img src="{{asset('client/images/slide.jpg')}}" alt="Nhà khách Hùng Vương 2">
            </div>
        @endif
    </div>
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#slide" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#slide" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
