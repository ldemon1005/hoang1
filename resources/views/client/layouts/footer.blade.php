<div class="container">
    <div class="row">
        <div class="col-md-4 footer-address">
            <h3>{{$config && $config->title != '' ? $config->title : 'TRUNG TÂM HỘI NGHỊ 37 HÙNG VƯƠNG'}}</h3>
            <p>Địa chỉ: {{$config && $config->address != '' ? $config->address : '37 Hùng Vương, Ba Đình, Hà Nội'}}</p>
            <p>Mail: {{$config && $config->email != '' ? $config->email : '37hungvuong@gmail.com'}}</p>
        </div>
        <div class="col-md-4 footer-connect">
            <p class="phone">
                <a href="#"><img src="{{asset('client/images/phone.png')}}">
                    {{$config && $config->phone != '' ? $config->phone : '0902.000.000'}}
                </a>
            </p>
            <p class="facebook"><a href="{{$config && $config->facebook != '' ? $config->facebook : '#'}}"><img src="{{asset('client/images/facebook.png')}}">Kết nối với chúng tôi !</a></p>
        </div>
        <div class="col-md-4 footer-link">
            <ul>
                <li><a href="{{route('introduction_view')}}" title="giới thiệu">Giới thiệu</a></li>
                <li><a href="{{route('list_post_view')}}" title="tin tức">Tin tức</a></li>
                <li><a href="{{route('list_partyroom_view')}}" title="tiệc">Tiệc</a></li>
                <li><a href="{{route('list_restroom_view')}}" title="phòng nghỉ">Phòng nghỉ</a></li>
                <li><a href="{{route('contact_view')}}" title="liên hệ">Liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
