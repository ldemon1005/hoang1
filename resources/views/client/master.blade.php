<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="John Doe">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="fb:app_id" content="522099905029641">
    <title>Nhà Khách Hùng Vương</title>
</head>
<body>
<!-- endbuild -->
@yield('client_css')
<link rel="stylesheet" href="{{asset('client/libs/bootstrap-4.0.0/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('client/libs/bootstrap-4.0.0/css/bootstrap-grid.css')}}">
<link rel="stylesheet" href="{{asset('client/libs/bootstrap-4.0.0/css/bootstrap-reboot.css')}}">
<link rel="stylesheet" href="{{asset('client/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
<link href="{{asset('admin/assets/plugins/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
<link href="{{asset('client/libs/slick/slick-theme.css')}}" rel="stylesheet">
<link href="{{asset('client/libs/slick/slick.css')}}" rel="stylesheet">
<!--alerts CSS -->
<link href="{{asset('admin/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="{{asset('client/libs/slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('client/libs/slick/slick-theme.css')}}">
<link rel="stylesheet" href="{{asset('client/libs/open-iconic/font/css/open-iconic.min.css')}}">

<link rel="stylesheet" href="{{asset('client/libs/fontawesome-5.10.2/css/all.min.css')}}">
<link href="{{asset('client/libs/menu/hc-offcanvas-nav.css')}}" rel="stylesheet">
<!-- build:css css/styles.min.css-->
<link rel="stylesheet" href="{{asset('client/css/style.css?v=1.1.2')}}">
<script src="https://kit.fontawesome.com/28e48251f0.js"></script>
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{asset('client/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/libs/bootstrap-4.0.0/js/bootstrap.bundle.js')}}"></script>
<script type="text/javascript" src="{{asset('client/libs/bootstrap-4.0.0/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('client/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('client/libs/menu/hc-offcanvas-nav.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('admin/assets/plugins/toast-master/js/jquery.toast.js')}}"></script>
<script src="{{asset('admin/main/js/toastr.js')}}"></script>
<!-- Sweet-Alert  -->
<script src="{{asset('admin/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="{{asset('admin/assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>

<!-- Facebook JavaScript SDK -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0&appId=522099905029641&autoLogAppEvents=1"></script>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=522099905029641";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- End Facebook JavaScript SDK -->

<!-- build:js js/custom.min.js-->
<!-- endbuild -->
<div class="common-overlay" onclick="hiddenMenuMobile()"></div>
<div class="box-main page-ttdn">
    @include('client.layouts.headerBar')
    @yield('content')
    <footer>
        @include('client.layouts.footer')
    </footer>
</div>

<script>
    $('.select2').select2();
</script>

<script src="{{asset('client/js/jquery.sticky.js')}}" type="text/javascript" charset="utf-8"></script>

<script src="{{asset('client/libs/slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    $(document).ready(function(){
        var $main_nav = $('#main-nav');
          var $toggle = $('.toggle-menu');

          var defaultData = {
            maxWidth: 780,
            customToggle: $toggle,
            navTitle: 'Danh mục',
            levelTitles: false,
            pushContent: '#container',
            insertClose: 2,
            closeLevels: false,
            labelClose: '',
            labelBack: 'Quay lại'
          };

          // add new items to original nav
          $main_nav.find('li.add').children('a').on('click', function() {
            var $this = $(this);
            var $li = $this.parent();
            var items = eval('(' + $this.attr('data-add') + ')');

            $li.before('<li class="new"><a>'+items[0]+'</a></li>');

            items.shift();

            if (!items.length) {
              $li.remove();
            }
            else {
              $this.attr('data-add', JSON.stringify(items));
            }

            Nav.update(true);
          });

          // call our plugin
          var Nav = $main_nav.hcOffcanvasNav(defaultData);

          // demo settings update

          const update = (settings) => {
            if (Nav.isOpen()) {
              Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
              });

              Nav.close();
            }
            else {
              Nav.update(settings);
            }
          };

          $('.actions').find('a').on('click', function(e) {
            e.preventDefault();

            var $this = $(this).addClass('active');
            var $siblings = $this.parent().siblings().children('a').removeClass('active');
            var settings = eval('(' + $this.data('demo') + ')');

            update(settings);
          });

          $('.actions').find('input').on('change', function() {
            var $this = $(this);
            var settings = eval('(' + $this.data('demo') + ')');

            if ($this.is(':checked')) {
              update(settings);
            }
            else {
              var removeData = {};
              $.each(settings, function(index, value) {
                removeData[index] = false;
              });

              update(removeData);
            }
        });

        $(".menu-mobile").sticky({ topSpacing: 0, zIndex: 999999 });

        $(".regular").slick({
            dots: true,
            autoplay:true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('#header-right .search').click(function(){
          $(this).css('display','none');
          $('#header-top').css('height', '50px');
          $('#header-right form').css('display', 'block');
        });

        $('#header-right .close').click(function(){
          $('#header-top').css('height', '23px');
          $('#header-right form').css('display', 'none');
          $('#header-right .search').css('display', 'block');
        });
    })
</script>
<script type="text/javascript">
    $('.date-come').datepicker("setDate", new Date());
    $('.date-go').datepicker("setDate", new Date());
    $('.date-come-2').datepicker("setDate", new Date());
    $('.date-go-2').datepicker("setDate", new Date());
    function checkNumberNights() {
        $('#date-come-2').val($('.date-come').val());
        $('#date-go-2').val($('.date-go').val());

        if($('.date-come').val() && $('.date-go').val()){
            if($('.date-come').val() < $('.date-go').val()){
                $('#number_nights').html('Ngày đến và đi không hợp lệ');
                $('.number-night').val(0);
            }else {
                $('#number_nights').html(datediff(parseDate($('.date-come').val()), parseDate($('.date-go').val())) + ' đêm');
                $('.number-night').val(datediff(parseDate($('.date-come').val()), parseDate($('.date-go').val())));
            }
        }
    }

    function checkNumberNights2() {
        $('#date-come-1').val($('#date-come-2').val());
        $('#date-go-1').val($('#date-go-2').val());

        if($('#date-come-2').val() && $('#date-go-2').val()){
            if($('#date-come-2').val() < $('#date-go-2').val()){
                $('#number_nights').html('Ngày đến và đi không hợp lệ');
                $('.number-night').val(0);
            }else {
                $('#number_nights').html(datediff(parseDate($('#date-come-2').val()), parseDate($('#date-go-2').val())) + ' đêm');
                $('.number-night').val(datediff(parseDate($('#date-come-2').val()), parseDate($('#date-go-2').val())));
            }
        }
    }

    function checkNumberNights3() {
        if($('#date-come-3').val() && $('#date-go-3').val()){
            if($('#date-come-3').val() < $('#date-go-3').val()){
                $('#number_nights').html('Ngày đến và đi không hợp lệ');
                $('.number-night').val(0);
            }else {
                $('#number_nights').html(datediff(parseDate($('#date-come-3').val()), parseDate($('#date-go-3').val())) + ' đêm');
                $('.number-night').val(datediff(parseDate($('#date-come-3').val()), parseDate($('#date-go-3').val())));
            }
        }
    }
    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[0]-1, mdy[1]);
    }

    function datediff(first, second) {
        return Math.round((first - second)/(1000*60*60*24));
    }

    function changeInfo(class_name) {
        var str = '.' + class_name;
        $(str).val($(str).val());
    }
</script>

@include('client.layouts.notification')
@yield('client_script')
</body>

</html>
