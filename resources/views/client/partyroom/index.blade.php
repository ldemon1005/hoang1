@extends('client.master')

@section('content')
    @include('client.layouts.banner_post',['title' => 'Phòng tiệc'])
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('index_view')}}"><span class="oi" data-glyph="home"></span>Trang chủ</a></li>
                    <li class="breadcrumb-item">Tiệc</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container" id="page-content">
      <div class="row">
        @foreach($partyRooms as $room)
        <div class="col-md-6 col-sm-6 col-xs-12 room-item">
          <img src="{{$room->image}}" alt="{{$room->title}}">
          <h3><a href="{{route('detail_partyroom_view',['slug' => $room->slug . '---' . $room->id])}}">{{$room->title}}</a></h3>
          <p class="description">{!! $room->description !!}</p>
          <a href="{{route('detail_partyroom_view',['slug' => $room->slug . '---' . $room->id])}}" class="btn btn-primary txt-right">Chi tiết +</a>
        </div>
        @endforeach
      </div>
    </div>
@endsection
