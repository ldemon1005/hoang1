@extends('client.master')

@section('content')
    @include('client.layouts.banner_post',['title' => 'Liên hệ'])
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('index_view')}}"><span class="oi" data-glyph="home"></span>Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liên hệ</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div id="page-contact" class="row">
            <div class="col-md-6">
                <div class="page-title">Thông tin liên hệ</div>
                <h3>{{$config ? $config->title : ''}}</h3>
                <p><b>Địa chỉ:</b> {{$config ? $config->address : ''}}</p>
                <p><b>Mail:</b> {{$config ? $config->email : ''}}</p>
                <p><b>Điện thoại:</b> {{$config ? $config->phone : ''}}</p>
                <p><b>Hotline:</b> {{$config ? $config->hotline : ''}}</p>
            </div>
            <div class="col-md-6">
                <h6>Liên hệ với chúng tôi</h6>
                <form action="{{route('contact_action')}}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="input-group mb-3 col-md-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input name="name" type="text" class="form-control" placeholder="Người liên hệ" required>
                        </div>
                        <div class="input-group mb-3 col-md-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
                            </div>
                            <input name="phone" type="text" class="form-control" placeholder="Điện thoại" required>
                        </div>
                        <div class="input-group mb-3 col-md-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-envelope"></i></span>
                            </div>
                            <input name="email" type="email" class="form-control" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea name="content" class="form-control mb-3" rows="5" id="content" placeholder="Nội dung" required></textarea>
                            <button type="submit" class="btn btn-secondary">Gửi tin nhắn</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="maps">
        <iframe
            src="{{$config->link_map && $config->link_map != '' ? $config->link_map : 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1861.9928451512594!2d105.8334182591273!3d21.03325856130094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aba1f135ec53%3A0x2f48e98ea3e53266!2zVHJ1bmcgdMOibSBI4buZaSBuZ2jhu4sgMzcgSMO5bmcgVsawxqFuZw!5e0!3m2!1svi!2s!4v1568642982039!5m2!1svi!2s'}}"
            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
@endsection

