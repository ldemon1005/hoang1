@extends('client.master')

@section('content')
    @include('client.layouts.banner')
    <div id="home-about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="title">
                        <h1>Welcome to <span>Hung Vuong Hotel</span></h1>
                        <div class="is-divider text-center bg-grey"></div>
                    </div>
                    <div class="content">
                        @if($config)
                            {!! $config->content_home !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="feature">
        <div class="container-fluid">
            <div class="row">
                @foreach($category_service as $category)
                    <div class="feature-item">
                        <div class="image" style="background-image:url({{$category->image}})">
                            <img src="{{$category->image}}" style="visibility: hidden;">
                        </div>
                        <div class="desc">
                            <h2 class="title bigTitle">{{$category->title}}</h2>
                            <div class="index">
                                {!! $category->content !!}
                                @if($category->id == 2)
                                    <a href="{{route('list_restroom_view')}}" class="more">Chi tiết</a>
                                @endif
                                @if($category->id == 3)
                                    <a href="{{route('list_restroom_view')}}" class="more">Chi tiết</a>
                                @endif
                            </div>
                            <div class="text"></div>
                        </div>
                        <div class="rollover"></div>
                        @if($category->id == 2)
                            <a href="{{route('list_partyroom_view')}}" class="bg-a"></a>
                        @endif
                        @if($category->id == 3)
                            <a href="{{route('list_restroom_view')}}" class="bg-a"></a>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="book">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="#" onclick="$('#consultant-restroom').modal('show')" data-toggle="modal" data-target="#consultant-resrroom"><img src="{{asset('client/images/dat-phong.png')}}"></a>
                    <a href="#" onclick="$('#consultant-partyroom').modal('show')" data-toggle="modal" data-target="#consultant-partyroom"><img src="{{asset('client/images/dat-tiec.png')}}"></a>
                </div>
            </div>
        </div>
    </div>

    <div class="container" id="news">
        <div class="col-md-12">
            <div class="title">Tin tức _ Sự kiện</div>
        </div>
        <section class="regular slider">
            @foreach($posts as $post)
                <div class="item">
                    <a href="{{route('detail_post_view',['slug' => $post->slug . '---' . $post->id])}}">
                        <div class="post-image">
                            <img src="{{$post->image}}" alt="{{$post->title}}">
                        </div>
                        <h4>{{$post->title}}</h4>
                    </a>
                    <p><span class="date">{{date('dd/mm/YY',$post->created_at)}}</span> | <span class="view">{{$post->total_view ? $post->total_view : 0}} view</span>
                    </p>
                </div>
            @endforeach
        </section>
    </div>

    <div class="modal" id="consultant-partyroom">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Đối với thông tin thêm, vui lòng liên hệ với chúng tôi tại đây</h4>
                    <form method="post" action="{{route('order_partyroom_view')}}">
                        {{csrf_field()}}
                        <input class="form-control" type="text" name="name" placeholder="Tên" required>
                        <input class="form-control" type="text" name="email" placeholder="Email" required>
                        <input class="form-control" type="text" name="phone" placeholder="Điện thoại" required>
                        <label class="book-demand">Nhu cầu đặt tiệc</label>
                        <div class="input-group date w45 fr">
                            <input class="form-control date-go-2" id="date-go-2" type="text" name="date_go" onchange="checkNumberNights2()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <div class="input-group date w45 ">
                            <input class="form-control date-come-2" id="date-come-2" type="text" name="date_come" onchange="checkNumberNights2()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <input class="form-control number-night" readonly name="number_nights">
                        <select class="room-type form-control" name="category_room">
                            <option>Loại phòng tiệc hội nghị</option>
                            @foreach($list_category_room as $category_room)
                                <option value="{{$category_room->id}}">{{$category_room->title}}</option>
                            @endforeach
                        </select>
                        <input class="room-person form-control" name="adults" placeholder="Người lớn">
                        <input class="room-baby form-control" name="children" placeholder="Trẻ em">
                        <button class="btn btn-secondary" type="submit">Gửi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="consultant-restroom">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Đối với thông tin thêm, vui lòng liên hệ với chúng tôi tại đây</h4>
                    <form method="post" action="{{route('order_restroom_view')}}">
                        {{csrf_field()}}
                        <input class="form-control" type="text" name="name" placeholder="Tên" required>
                        <input class="form-control" type="email" name="email" placeholder="Email" required>
                        <input class="form-control" type="text" name="phone" placeholder="Điện thoại" required>
                        <label class="book-demand">Nhu cầu đặt phòng</label>
                        <div class="input-group date w45 fr">
                            <input class="form-control date-go-2" id="date-go-3" type="text" name="date_go" onchange="checkNumberNights3()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <div class="input-group date w45 ">
                            <input class="form-control date-come-2" id="date-come-3" type="text" name="date_come" onchange="checkNumberNights3()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <input class="form-control number-night" readonly name="number_nights">
                        <select class="room-type form-control" name="category_room">
                            <option>Loại phòng</option>
                            @foreach($list_category_room as $category_room)
                                <option value="{{$category_room->id}}">{{$category_room->title}}</option>
                            @endforeach
                        </select>
                        <input class="room-person form-control" type="number" min="0" name="adults" placeholder="Người lớn">
                        <input class="room-baby form-control" type="number" min="0" name="children" placeholder="Trẻ em">
                        <button class="btn btn-secondary" type="submit">Gửi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

