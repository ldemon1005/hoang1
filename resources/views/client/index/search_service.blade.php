@extends('client.master')

@section('content')
    @include('client.layouts.banner_post',['title' => 'Dịch vụ'])

    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('index_view')}}"><span class="oi" data-glyph="home"></span>Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Dịch vụ</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div id="page-content" class="row">
            <div class="col-md-12">
                <div class="row" id="room-item">
                    @foreach($services as $service)
                        <div class="col-md-3">
                            <img src="{{$service->image}}">
                            <h3><a href="#">{{$service->title}}</a></h3>
                            <p class="description">{{$service->description}}</p>
                            <a href="{{route( $service->category == \App\Models\Service::RESTROOM ? 'detail_restroom_view' : 'detail_partyroom_view',['slug' => $service->slug . '---' . $service->id])}}" class="btn btn-primary">Chi tiết +</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
