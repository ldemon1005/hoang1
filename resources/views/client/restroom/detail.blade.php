@extends('client.master')

@section('content')
    @include('client.layouts.banner_post',['title' => 'Phòng nghỉ'])
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('index_view')}}"><span class="oi" data-glyph="home"></span>Trang chủ</a></li>
                    <li class="breadcrumb-item"><a href="{{route('list_restroom_view')}}" title="Phòng nghỉ">Phòng nghỉ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$restroom->title}}</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div id="room-detail" class="row">
            <div class="col-md-12">
                {!! $restroom->content !!}
                <div class="contact-book-room">
                    <p class="title">
                        Để đặt phòng, xin vui lòng liên hệ:
                    </p>
                    <p class="hotline">
                        Hottline: <span class="big-text">{{$config ? $config->hotline : ''}}</span> <br/>
                        Hoặc gửi yêu cầu của bạn <a href="#" onclick="$('#consultant').modal('show')" data-toggle="modal" data-target="#consultant">tại đây</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="consultant">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Đối với thông tin thêm, vui lòng liên hệ với chúng tôi tại đây</h4>
                    <form method="post" action="{{route('order_restroom_view')}}">
                        {{csrf_field()}}
                        <input class="form-control" type="text" name="name" placeholder="Tên" required>
                        <input class="form-control" type="email" name="email" placeholder="Email" required>
                        <input class="form-control" type="text" name="phone" placeholder="Điện thoại" required>
                        <label class="book-demand">Nhu cầu đặt phòng</label>
                        <div class="input-group date w45 fr">
                            <input class="form-control date-go-2" id="date-go-2" type="text" name="date_go" onchange="checkNumberNights2()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <div class="input-group date w45 ">
                            <input class="form-control date-come-2" id="date-come-2" type="text" name="date_come" onchange="checkNumberNights2()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <input class="form-control number-night" readonly name="number_nights">
                        <select class="room-type form-control" name="category_room">
                            <option>Loại phòng</option>
                            @foreach($list_category_room as $category_room)
                                <option value="{{$category_room->id}}">{{$category_room->title}}</option>
                            @endforeach
                        </select>
                        <input class="room-person form-control" type="number" min="0" name="adults" placeholder="Người lớn">
                        <input class="room-baby form-control" type="number" min="0" name="children" placeholder="Trẻ em">
                        <button class="btn btn-secondary" type="submit">Gửi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
