@extends('client.master')

@section('content')
    @include('client.layouts.banner_post',['title' => 'Phòng nghỉ'])

    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('index_view')}}"><span class="oi" data-glyph="home"></span>Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Phòng nghỉ</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div id="page-content" class="row">
            <div class="col-md-12">
                <div class="box-filter">
                    <div class="form-group come-go">
                        <label for="email" class="fl">Ngày: <span id="number_nights">0 đêm</span></label>
                        <div class="input-group w50 input-group-first">
                            <i class="fas fa-calendar-alt"></i>
                            <input type="text" class="form-control date-go" id="date-go-1"
                                   onchange="checkNumberNights()" placeholder="Ngày đi">
                        </div>
                        <div class="input-group w50">
                            <i class="fas fa-calendar-alt"></i>
                            <input type="text" class="form-control date-come" id="date-come-1"
                                   onchange="checkNumberNights()" placeholder="Ngày đến">
                        </div>
                    </div>
                    <div class="form-group room">
                        <label for="email">Phòng</label>
                        <select class="form-control">
                            <option>Loại phòng</option>
                            @foreach($list_category_room as $category_room)
                                <option value="{{$category_room->id}}">{{$category_room->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group guest">
                        <label>Người lớn</label>
                        <input class="room-person form-control" onchange="changeInfo('room-person')">
                    </div>
                    <div class="form-group guest">
                        <label>Trẻ con</label>
                        <input class="room-baby form-control" onchange="changeInfo('room-baby')">
                    </div>
                    <div class="form-group btn-book">
                        <label style="color: #f4f4f4">Đặt phòng</label>
                        <button type="button" class="btn btn-primary" data-toggle="modal" onclick="$('#consultant').modal('show')" data-target="#consultant">Đặt
                            phòng
                        </button>
                    </div>
                </div>
                <div class="page-description">
                    Nhà khách 37 Hùng Vương nổi bật với phong cách thiết kế hiện đại, rất nhẹ nhàng nhưng cũng rất sang
                    trọng, tinh tế, đầy tính nghệ thuật. Với thế mạnh các món ăn vừa ngon miệng vừa đẹp mẳt, không gian
                    tinh tế và giá thành hợp lý, là một lựa chọn lý tưởng cho quý khách cùng chia sẻ
                    những khoảnh khắc ấm cúng của mình giữa lòng thủ đô Hà Nội.
                </div>
                <div class="row">
                    @foreach($restrooms as $restroom)
                        <div class="col-md-3 room-item">
                            <img src="{{$restroom->image}}" title="{{$restroom->title}}">
                            <h3><a href="{{route('detail_restroom_view',['slug' => $restroom->slug . '---' . $restroom->id])}}" title="{{$restroom->title}}">{{$restroom->title}}</a></h3>
                            <p class="description">{{$restroom->description}}</p>
                            <a href="{{route('detail_restroom_view',['slug' => $restroom->slug . '---' . $restroom->id])}}" class="btn btn-primary">Chi tiết +</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="consultant">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Đối với thông tin thêm, vui lòng liên hệ với chúng tôi tại đây</h4>
                    <form method="post" action="{{route('order_restroom_view')}}">
                        {{csrf_field()}}
                        <input class="form-control" type="text" name="name" placeholder="Tên" required>
                        <input class="form-control" type="email" name="email" placeholder="Email" required>
                        <input class="form-control" type="text" name="phone" placeholder="Điện thoại" required>
                        <label class="book-demand">Nhu cầu đặt phòng</label>
                        <div class="input-group date w45 fr">
                            <input class="form-control date-go-2" id="date-go-2" type="text" name="date_go" onchange="checkNumberNights2()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <div class="input-group date w45 ">
                            <input class="form-control date-come-2" id="date-come-2" type="text" name="date_come" onchange="checkNumberNights2()">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <input class="form-control number-night" readonly name="number_nights">
                        <select class="room-type form-control" name="category_room">
                            <option>Loại phòng</option>
                            @foreach($list_category_room as $category_room)
                                <option value="{{$category_room->id}}">{{$category_room->title}}</option>
                            @endforeach
                        </select>
                        <input class="room-person form-control" type="number" min="0" name="adults" placeholder="Người lớn">
                        <input class="room-baby form-control" type="number" min="0" name="children" placeholder="Trẻ em">
                        <button class="btn btn-secondary" type="submit">Gửi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
