@extends('client.master')

@section('content')
    @include('client.layouts.banner_post',['title' => 'Tin tức'])

    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('index_view')}}"><span class="oi" data-glyph="home"></span>Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tin tức</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div id="news" class="row page-news">
            @if(count($posts) == 0)
                Không có bài viết nào!!!
            @endif
            @foreach($posts as $post)
                <div class="col-xs-12 col-sm-4 col-md-3 item-news">
                    <div class="item">
                        <a href="{{route('detail_post_view',['slug' => $post->slug . '---' . $post->id])}}">
                            <div class="post-image">
                                <img src="{{$post->image}}" alt="{{$post->title}}">  
                            </div>
                            <h4>{{$post->title}}</h4>
                        </a>
                        <p><span class="date">{{date('d/m/Y', $post->created_at)}}</span>  |  <span class="view">{{$post->total_view ? $post->total_view : 0}} view</span></p>
                        <hr/>
                        <p class="desc">
                            {!! $post->description !!}
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="text-center">
                {!! $posts->links() !!}
            </div>
        </div>
    </div>
@endsection

@section('client_script')
    <script>
        $('.pagination a').unbind('click').on('click', function(e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            getPosts(page);
        });

        function getPosts(page)
        {
            $.ajax({
                type: "GET",
                url: '?page='+ page,
                success: function(data) {
                    $('body').html(data);
                }
            })
        }
    </script>
@endsection

