@extends('client.master')

@section('content')
    @include('client.layouts.banner_post',['title' => 'Tin tức'])
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('index_view')}}"><span class="oi" data-glyph="home"></span>Trang chủ</a></li>
                    <li class="breadcrumb-item">Tin tức</li>
                    <li class="breadcrumb-item active" aria-current="page">{{$post->title}}</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div id="news" class="row">
            <div class="col-md-9">
                <h1>{{$post->title}}</h1>
                <p><span class="date">{{date('d/m/Y',$post->created_at)}}</span>  |  <span class="view">{{$post->total_view}} view</span></p>
                <div class="news-content">
                    {!! $post->content !!}
                </div>
                <div id="comments" class="comments-area">
                    <div class="fb-comments" data-href="{{route('detail_post_view',['slug' => $post->slug . '---' . $post->id])}}" data-colorscheme="light" data-numposts="5" data-width="100%"></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="news-item-title">
                    Tin tức khác
                </div>
                <div class="news-item">
                    @foreach($post_relate as $relate)
                        <div class="item">
                            <a href="{{route('detail_post_view',['slug' => $relate->slug . '---' . $relate->id])}}">
                                <img src="{{$relate->image}}">
                                <h4>{{$relate->title}}</h4>
                            </a>
                            <p><span class="date">{{date('d/m/Y', $relate->created_at)}}</span>  |  <span class="view">{{$relate->total_view ? $relate->total_view : 0}} view</span></p>
                            <hr/>
                            <p class="desc">
                                {!! $relate->description !!}
                            </p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal" id="consultant">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Đối với thông tin thêm, vui lòng liên hệ với chúng tôi tại đây</h4>
                    <form>
                        <input class="form-control" type="text" name="name" placeholder="Tên">
                        <input class="form-control" type="text" name="email" placeholder="Email">
                        <input class="form-control" type="text" name="phone" placeholder="Điện thoại">
                        <label class="book-demand">Nhu cầu đặt phòng</label>
                        <div class="input-group date w45 ">
                            <input class="form-control date-come" type="text" name="date-come" class="date-come">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <div class="input-group date w45 fr">
                            <input class="form-control date-go" type="text" name="date-go" class="date-go">
                            <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <select class="room-type form-control" name="room-type">
                            <option>Loại phòng</option>
                            <option>Loại 1</option>
                            <option>Loại 2</option>
                        </select>
                        <select class="room-person form-control" name="room-person">
                            <option>Người lớn</option>
                            <option>Người lớn 1</option>
                            <option>Người lớn 2</option>
                        </select>
                        <select class="room-baby form-control" name="room-baby">
                            <option>Trẻ em</option>
                            <option>Trẻ em 1</option>
                            <option>Trẻ em 2</option>
                        </select>
                        <button class="btn btn-secondary" type="submit">Gửi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

