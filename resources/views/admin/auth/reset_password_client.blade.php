@extends('admin.master')

@section('content')
    <div class="container" style="padding-top: 50px;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Đổi mật khẩu</div>

                    <div class="panel-body">
                        <form  action="{{route('admin_auth_reset_password_action')}}" method="post" enctype="multipart/form-data" id="reset_password">
                            {{csrf_field()}}
                            <div class="row form-group">
                                <div id="Post_Content" class="col-12 col-md-8">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title font-bold">ĐỔI MẬT KHẨU ADMIN</h4>
                                            <div class="form-group">
                                                <label for="user_title">Mật khẩu cũ <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="current_password" id="current_password" aria-describedby="helpPostTitle" placeholder="Nhập mật khẩu cũ"
                                                       autocomplete="off" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_title">Mật khẩu mới <span class="text-danger">*</span></label>
                                                <input type="password" class="form-control" name="new_password" id="new_password" aria-describedby="helpPostTitle" placeholder="Nhập mật khẩu mới"
                                                       autocomplete="off" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_title">Nhập lại mật khẩu <span class="text-danger">*</span></label>
                                                <input type="password" class="form-control" name="re_password_new" id="re_password_new" aria-describedby="helpPostTitle" placeholder="Nhập lại mật khẩu mới"
                                                       autocomplete="off" required>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Đổi mật khẩu</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
