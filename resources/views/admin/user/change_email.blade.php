@extends('admin.master')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form  action="{{route('admin_auth_reset_password_action')}}" method="post" enctype="multipart/form-data" id="reset_password">
            {{csrf_field()}}
            <input hidden name="id" value="{{$user->id}}">
            <div class="row form-group">
                <div id="Post_Content" class="col-12 col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG ADMIN</h4>
                            <div class="form-group">
                                <label for="user_title">Mật khẩu cũ <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="password" id="password" aria-describedby="helpPostTitle" placeholder="Nhập mật khẩu cũ"
                                       autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label for="user_title">Mật khẩu mới <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" name="password_new" id="password_new" aria-describedby="helpPostTitle" placeholder="Nhập mật khẩu mới"
                                       autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label for="user_title">Nhập lại mật khẩu <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" name="re_password_new" id="re_password_new" aria-describedby="helpPostTitle" placeholder="Nhập lại mật khẩu mới"
                                       autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>

    <script>
        $().ready(function() {
            $("#reset_password").validate({
                onfocusout: false,
                onkeyup: false,
                onclick: false,
                rules: {
                    "password": {
                        required: true,
                        maxlength: 15
                    },
                    "password_new": {
                        required: true
                    },
                    "re_password_new": {
                        equalTo: "#password_new",
                    }
                }
            });
        });
    </script>
@endsection
