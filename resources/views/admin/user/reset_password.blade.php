@extends('admin.master')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form  action="{{route('admin_update_user_action')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input hidden name="id" value="{{$user->id}}">
            <div class="row form-group">
                <div id="Post_Content" class="col-12 col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN ADMIN</h4>
                            <div class="form-group">
                                <label for="user_title">Tên <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" id="user_email" aria-describedby="helpPostTitle" placeholder="Nhập email"
                                       autocomplete="off" value="{{$user->title}}" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
