@extends('admin.master')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form  action="{{$categoryService ? route('admin_update_category_service_action') : route('admin_create_category_service_action')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($categoryService)
                <input hidden name="id" value="{{$categoryService->id}}">
            @endif
            <div class="row form-group">
                <div id="Post_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN DANH MỤC DỊCH VỤ</h4>
                            <div class="form-group">
                                <label for="categoryService_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" id="categoryService_title" aria-describedby="helpPostTitle" placeholder="Nhập tiêu dịch vụ"
                                       autocomplete="off" value="{{$categoryService ? $categoryService->title : ''}}" required>
                                <small id="helpPostTitle" class="form-text text-muted">Tiêu dịch vụ là bắt buộc</small>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.layouts.image_preview', ['input_name' => 'image', 'input_id' => 'categoryService_image', 'input_image' => $categoryService ? $categoryService->image : ''])
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row form-group" style="padding: 10px 0">
                                <div class="col-md-2">
                                    <label>Loại dịch vụ</label>
                                </div>
                                <div class="col-md-8">
                                    <select class="form-control" name="type">
                                        <option {{$categoryService && $categoryService->type == 1 ? 'checked' : ''}} value="1">Dịch vụ cho thuê</option>
                                        <option {{$categoryService && $categoryService->type == 2 ? 'checked' : ''}} value="2">Dịch vụ kèm thêm</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group" style="padding: 10px 0">
                                <div class="col-md-2">
                                    <label>Trạng thái</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" {{$categoryService && $categoryService->status == 1 ? 'checked' : ''}}>
                                        <label class="custom-control-label" for="customSwitch1">Hoạt động</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="post_content">Nội dung</label>
                                <textarea class="form-control mce_editor" name="content" id="post_content" rows="3">{{$categoryService ? $categoryService->content : ''}}</textarea>
                            </div>
                            <div class="row form-group" style="padding: 10px 0">
                                <div class="col-md-2">
                                    <label>Sắp xếp</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="order" value="{{$categoryService && $categoryService->order ? $categoryService->order : ''}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>

                            <button type="submit" class="btn btn-info"> Lưu</button>
                            <a type="button" class="btn btn-danger" href="{{route('admin_list_category_service')}}"> Hủy</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
