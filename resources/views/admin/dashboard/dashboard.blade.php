@extends('admin.master')

@section('content')
    <div>

        <!-- Main content -->
        <section class="content">
            <p>Chào mừng bạn quay trở lại với hệ thống</p>

            @foreach($posts as $post)
                <div class="row">
                    <label class="col-md-4">{{$post->status == 1 ? 'Số bài đang đăng' : 'Số bài viết không đăng'}}</label>
                    <div class="col-md-8">
                        {{$post->total}}
                    </div>
                </div>
            @endforeach

            @foreach($orders as $order)
                <div class="row">
                    <label class="col-md-4">{{$order->type == 3 ? 'Số phòng nghỉ được đặt' : 'Số phòng tiệc được đặt'}}</label>
                    <div class="col-md-8">
                        {{$order->total}}
                    </div>
                </div>
            @endforeach
        </section>
        <!-- /.content -->
    </div>
@endsection
