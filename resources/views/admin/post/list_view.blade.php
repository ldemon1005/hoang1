@extends('admin.master')

@section('content')
    <div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Danh sách tin tức
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Danh sách tin tức</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row form-group">
                <form method="get" action="{{route('admin_list_post')}}" id="search-form">
                    <div class="col-md-3">
                        <div class="input-group">
                            <input type="text" name="keyword" value="{{$params['keyword']}}" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" onchange="$('#search-form').submit()" name="status">
                            <option value="9" selected>Tất cả</option>
                            <option {{isset($params['status']) && $params['status'] == 1 ? 'selected' : ''}} value="1">Hoạt động</option>
                            <option {{isset($params['status']) && $params['status'] == 0 ? 'selected' : ''}} value="0">Không hoạt động</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" onchange="$('#search-form').submit()" name="category">
                            <option value="" selected>Tất cả danh mục</option>
                            @foreach($list_category as $category)
                                <option {{isset($params['category']) && $params['category'] == $category->id ? 'selected' : ''}} value="{{$category->id}}">{{$category->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
                <div class="col-md-3 text-right">
                    <a type="button" id="add-post" class="btn btn-primary" href="{{route('admin_update_post_view',['id' => 0])}}">Thêm mới</a>
                </div>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Danh mục</th>
                    <th style="width: 10%;text-align: center">Status</th>
                    <th style="width: 15%;text-align: center">Action</th>
                </tr>
                @foreach($list_post as $key => $post)
                    <tr>
                        <td>{{$key +1}}.</td>
                        <td>{{$post->title}}</td>
                        <td><img height="30" class="image_preview" src="{{$post->image ? $post->image : asset('img/placeholder.png')}}"></td>
                        <td>{{$post->getCategory ? $post->getCategory->title : ''}}</td>
                        <td class="text-center">
                            <button type="button" class="btn btn-{{$post->status == 1 ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$post->status == 1 ? 'Hoạt động' : 'Không hoạt động'}}">
                                <i class="fa fa-{{$post->status == 1 ? 'eye' : 'eye-slash'}}"></i>
                            </button>
                        </td>
                        <td class="text-center">
                            <a type="button" class="btn btn-success btn-rounded btn-sm btn__edit" href="{{route('admin_update_post_view', ['id' => $post->id])}}" data-toggle="tooltip" title="Chỉnh sửa">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-post_id="{{$post->id}}" data-toggle="tooltip" title="Xoá">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                <div class="row form-group pull-right">
                    {{$list_post->links()}}
                </div>
            </table>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('admin_script')
    <script>
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let post_id = $(this).attr("data-post_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_delete_post')}}",
                        data: {
                            id: post_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr);
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection
