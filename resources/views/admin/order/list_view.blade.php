@extends('admin.master')

@section('content')
    <div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Danh sách đặt phòng
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Danh sách đặt phòng</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row form-group">
                <form method="get" action="{{route('admin_list_order')}}" id="search-form">
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" name="keyword" value="{{$params['keyword']}}" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" onchange="$('#search-form').submit()" name="status">
                            <option value="9" selected>Tất cả</option>
                            <option {{isset($params['status']) && $params['status'] == 1 ? 'selected' : ''}} value="1">Đã xử lý</option>
                            <option {{isset($params['status']) && $params['status'] == 0 ? 'selected' : ''}} value="0">Chưa xử lý</option>
                        </select>
                    </div>
                </form>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Tên</th>
                    <th>Số điện thoại</th>
                    <th>Email</th>
                    <th>Loại</th>
                    <th style="width: 10%;text-align: center">Status</th>
                    <th style="width: 15%;text-align: center">Action</th>
                </tr>
                @foreach($list_order as $key => $order)
                    <tr>
                        <td>{{$key +1}}.</td>
                        <td>{{$order->name}}</td>
                        <td>{{$order->phone}}</td>
                        <td>{{$order->email}}</td>
                        <td>{{$order->getCategory ? $order->getCategory->title : ''}}</td>
                        <td class="text-center">
                            <button type="button" class="btn btn-{{$order->status == 1 ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$order->status == 1 ? 'Đã xử lý' : 'Chưa xử lý'}}">
                                <i class="fa fa-{{$order->status == 1 ? 'eye' : 'eye-slash'}}"></i>
                            </button>
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                    data-order_id="{{$order->id}}">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-order_id="{{$order->id}}" data-toggle="tooltip" title="Xoá">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                <div class="row form-group pull-right">
                    {{$list_order->links()}}
                </div>
            </table>
        </section>
        <!-- /.content -->

        <div class="modal" ref="order" id="update-order">
        </div>
    </div>
@endsection

@section('admin_script')
    <script>
        $(document).ready(function () {
            $('#add-order').on('click', function (e) {
                $('#order').modal();
            });

            $('.btn__delete').on("click", function (e) {
                let order_id = $(this).attr("data-order_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_delete_order')}}",
                        data: {
                            id: order_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr);
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });

            $('.btn__edit').on("click", function (e) {
                let order_id = $(this).attr("data-order_id");
                $.ajax({
                    type: "GET",
                    url:  "update-order/" + order_id,
                    dataType: "json",
                    success: function (result) {
                        if (result.code === 1) {
                            $('#update-order').html(result.data);
                            $('#update-order').modal();
                        } else {
                            toastError(result.msg);
                        }
                    },
                    error: function (xhr) {
                        toastError(xhr.responseJSON.msg);
                    }
                });
            });
        });
    </script>
@endsection
