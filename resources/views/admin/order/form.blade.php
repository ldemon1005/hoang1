<div class="modal-dialog">
    <div class="modal-content">
        <form method="post" action="{{route('admin_update_order_action')}}">
            {{csrf_field()}}
            <input hidden value="{{$order->id}}" name="id">
            <div class="modal-header">
                <h4 class="modal-title">Chi tiết đặt phòng</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Họ tên</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name" value="{{$order->name}}"/>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Email</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="email" value="{{$order->email}}"/>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Số điện thoại</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="phone" value="{{$order->phone}}"/>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Số người lớn</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="adults" value="{{$order->adults}}"/>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Số trẻ em</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="children" value="{{$order->children}}"/>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Từ ngày</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="date_go" value="{{date('d/m/Y',$order->date_go)}}"/>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Tới ngày</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="date_come" value="{{date('d/m/Y',$order->date_come)}}"/>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Loại dịch vụ</label>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control" name="type">
                            <option>Dịch vụ</option>
                            @foreach($list_category_service as $category_service)
                                <option {{$order->type == $category_service->id ? 'selected' : ''}}  value="{{$category_service->id}}">{{$category_service->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Hạng dịch vụ</label>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control" name="category_room">
                            <option>Hạng</option>
                            @foreach($list_category_room as $category_room)
                                <option {{$order->category_room == $category_room->id ? 'selected' : ''}} value="{{$category_room->id}}">{{$category_room->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Trạng thái</label>
                    </div>
                    <div class="col-md-8">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" {{$order->status == 1 ? 'checked' : ''}}>
                            <label class="custom-control-label" for="customSwitch1">Đã xử lý</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-primary btn-add-order">Lưu</button>
            </div>
        </form>
    </div>
</div>
